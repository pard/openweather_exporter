use clap::{crate_authors, crate_name, crate_version, Arg};
#[macro_use]
extern crate failure;
use hyper::{Body, Request};
use log::{info, trace};
mod openweather_metrics;
use openweather_metrics::generate_metrics;
extern crate openweather_rs;
use openweather_rs::get_weather;
mod options;
use options::Options;
use prometheus_exporter_base::render_prometheus;
use std::env;
use std::net::IpAddr;
use std::sync::Arc;

async fn perform_request(
    _req: Request<Body>,
    options: Arc<Options>
    ) -> Result<String, failure::Error> {

    let api_key = options.api_key.to_owned();
    let units = options.units.to_owned();
    let zip_code = options.zip_code.to_owned();

    let weather = get_weather(
        api_key,
        zip_code.clone(),
        units.clone(),
        ).await.unwrap();


    Ok(generate_metrics(zip_code, units, weather))

}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = clap::App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!("\n"))
        .arg(
            Arg::with_name("addr")
                .short("l")
                .help("exporter address")
                .default_value("0.0.0.0")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("port")
            .short("p")
            .help("exporter port")
            .default_value("33133")
            .takes_value(true),
        )
        .arg(
            Arg::with_name("verbose")
            .short("v")
            .help("verbose logging")
            .takes_value(false),
        )
        .arg(
            Arg::with_name("zip_code")
            .short("z")
            .help("zip code")
            .required(true)
            .takes_value(true),
        )
        .arg(
            Arg::with_name("units")
            .short("u")
            .help("unit type")
            .default_value("metric")
            .possible_values(&["default", "metric", "imperial"])
            .takes_value(true),
        )
        .arg(
            Arg::with_name("api_key")
            .short("k")
            .help("api key")
            .required(true)
            .takes_value(true),
        )
        .get_matches();

    if matches.is_present("verbose") {
        env::set_var(
            "RUST_LOG",
            format!("folder_size=trace,{}=trace", crate_name!()),
        );
    } else {
        env::set_var(
            "RUST_LOG",
            format!("folder_size=info,{}=info", crate_name!()),
        );
    }
    env_logger::init();
    let options = Options::from_claps(&matches);
    info!("using matches: {:?}", matches);

    let port = u16::from_str_radix(
        &matches.value_of("port").unwrap(),
        10).expect("port must be a valid number");
    let ip = matches.value_of("addr")
        .unwrap()
        .parse::<IpAddr>().unwrap();
    let addr = (ip, port).into();
    println!("starting exporter on {}", addr);

    render_prometheus(addr, options, |request, options| {
        Box::pin(perform_request(request, options))
    })
    .await;

    Ok(())
}
