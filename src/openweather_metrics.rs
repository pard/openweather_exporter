use prometheus_exporter_base::{MetricType, PrometheusMetric};
use openweather_rs::open_weather_types::OpenWeather;

pub fn generate_metrics(zip_code: String, units: String, weather: OpenWeather) -> String {
    let timestamp = weather.dt.to_string();
    let units = match units.as_str() {
        "default" => "kelvin",
        "imperial" => "imperial",
        "metric" => "metric",
        _ => "unknown",
    };
    let mut s = String::new();

    let pc_curr_temp = PrometheusMetric::new(
        "openweather_current_temperature",
        MetricType::Gauge,
        "Current temperature"
        );
    let mut s_curr_temp = pc_curr_temp.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    attributes.push(("unit", units));
    s_curr_temp.push_str(&pc_curr_temp.render_sample(Some(&attributes), weather.main.temp, None));
    s.push_str(&s_curr_temp);

    let pc_curr_pressure = PrometheusMetric::new(
        "openweather_current_pressure",
        MetricType::Gauge,
        "Current pressure"
        );
    let mut s_curr_pressure = pc_curr_pressure.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    attributes.push(("unit", "hPa"));
    s_curr_pressure.push_str(&pc_curr_pressure.render_sample(Some(&attributes), weather.main.pressure, None));
    s.push_str(&s_curr_pressure);

    let pc_curr_humidity = PrometheusMetric::new(
        "openweather_current_humidity_pct",
        MetricType::Gauge,
        "Current humidity"
        );
    let mut s_curr_humidity = pc_curr_humidity.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    s_curr_humidity.push_str(&pc_curr_humidity.render_sample(Some(&attributes), weather.main.humidity, None));
    s.push_str(&s_curr_humidity);

    let pc_curr_clouds = PrometheusMetric::new(
        "openweather_current_clouds_pct",
        MetricType::Gauge,
        "Current clouds"
        );
    let mut s_curr_clouds = pc_curr_clouds.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    s_curr_clouds.push_str(&pc_curr_clouds.render_sample(Some(&attributes), weather.clouds.all, None));
    s.push_str(&s_curr_clouds);

    let pc_rain_last_hour = PrometheusMetric::new(
        "openweather_rain_last_hour",
        MetricType::Gauge,
        "Rain fall in the last hour"
        );
    let mut s_rain_last_hour = pc_rain_last_hour.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    attributes.push(("unit", "millimeters"));
    s_rain_last_hour.push_str(
        &pc_rain_last_hour.render_sample(Some(&attributes),
        weather.rain.one_hour,
        None));
    s.push_str(&s_rain_last_hour);

    let pc_rain_last_three_hours = PrometheusMetric::new(
        "openweather_rain_last_three_hours",
        MetricType::Gauge,
        "Rain fall in the last three hours"
        );
    let mut s_rain_last_three_hours = pc_rain_last_three_hours.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    attributes.push(("unit", "millimeters"));
    s_rain_last_three_hours.push_str(
        &pc_rain_last_three_hours.render_sample(Some(&attributes),
        weather.rain.three_hour,
        None
        ));
    s.push_str(&s_rain_last_three_hours);

    let pc_snow_last_hour = PrometheusMetric::new(
        "openweather_snow_last_hour",
        MetricType::Gauge,
        "snow fall in the last hour"
        );
    let mut s_snow_last_hour = pc_snow_last_hour.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    attributes.push(("unit", "millimeters"));
    s_snow_last_hour.push_str(
        &pc_snow_last_hour.render_sample(Some(&attributes),
        weather.snow.one_hour,
        None));
    s.push_str(&s_snow_last_hour);

    let pc_snow_last_three_hours = PrometheusMetric::new(
        "openweather_snow_last_three_hours",
        MetricType::Gauge,
        "snow fall in the last three hours"
        );
    let mut s_snow_last_three_hours = pc_snow_last_three_hours.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    attributes.push(("unit", "millimeters"));
    s_snow_last_three_hours.push_str(
        &pc_snow_last_three_hours.render_sample(Some(&attributes),
        weather.snow.three_hour,
        None
        ));
    s.push_str(&s_snow_last_three_hours);

    let pc_curr_visibility = PrometheusMetric::new(
        "openweather_current_visibility",
        MetricType::Gauge,
        "Current visibility"
        );
    let mut s_curr_visibility = pc_curr_visibility.render_header();
    let mut attributes = Vec::new();
    attributes.push(("zip_code", zip_code.as_str()));
    attributes.push(("unit", "meters"));
    s_curr_visibility.push_str(
        &pc_curr_visibility.render_sample(Some(&attributes),
        weather.visibility,
        None
        ));
    s.push_str(&s_curr_visibility);

    s
}
