#[derive(Debug, Clone)]
pub(crate) struct Options {
    pub api_key: String,
    pub units: String,
    pub zip_code: String,
}

impl Options {
    pub fn from_claps(matches: &clap::ArgMatches<'_>) -> Options {
        Options {
            api_key: matches.value_of("api_key").unwrap().to_string(),
            units: matches.value_of("units").unwrap().to_string(),
            zip_code: matches.value_of("zip_code").unwrap().to_string(),
        }
    }
}
