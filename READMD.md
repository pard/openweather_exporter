# openweather_metrics

A Prometheus exporter for weather data provided by Openweathermaps.org

## Usage

```shell
./openweather_metrics --port 12345 --api_key $API_KEY --zip_code 12345 --units imperial
```

Or use Docker:
```shell
docker run -p 9101:33133 pard68/openweather_metrics --port 12345 --api_key $API_KEY --zip_code 12345 --units imperial
```
